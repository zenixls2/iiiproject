import time
import operations

monitor_interval = 5

def migration_scheduling(node_counters, node_contrib, node_access):
    pass

def heartbeatCollector(node_counters, node_names):
    for node in node_counters:
        node.getdriver()
    prev_time = time.time()
    node_contrib = {}
    for i in node_counters:
        node_contrib[i.name] = 0
    while True:
        starttime = time.time()
        boundary_ops = []
        node_access = {}
        io = ro = uo = so = do = 0
        for i in node_counters:
            node_access[i.name] = 0
            with i.insertlock:
                io += i.insert_o.value
                node_contrib[i.name] += i.insert_o.value
                node_access[i.name] += i.insert_o.value
            with i.readlock:
                ro += i.read_o.value
                node_contrib[i.name] += i.read_o.value
                node_access[i.name] += i.read_o.value
            with i.updatelock:
                uo += i.update_o.value
                node_contrib[i.name] += i.update_o.value
                node_access[i.name] += i.update_o.value
            with i.scanlock:
                so += i.scan_o.value
                node_contrib[i.name] += i.scan_o.value
                node_access[i.name] += i.scan_o.value
            with i.deletelock:
                do += i.delete_o.value
                node_contrib[i.name] += i.delete_o.value
                node_access[i.name] += i.delete_o.value
        for node in node_counters:
            node.monitor()
        migration_scheduling(node_counters, node_contrib, node_access)
        prev_time = time.time()
        if time.time() - starttime <= monitor_interval:
            time.sleep(monitor_interval - time.time() + starttime)
