#!/usr/bin/env python
import socketrpc.server as server
import coordinator.coordImp as imp
import coordinator.heartbeatImp as himp
import sys
from dbtypes import *

def parseLine(line, tf):
    """
    Parse config line
    @return - (db_name, driver_instance) (tuple)
    Parameter:
     - line: line to read (str)
     - tf: should the driver only work as a driver to disable some initialization. (bool)
    """
    row = line.split()
    if len(row) == 4:
        name = row[0]
        ip = row[1]
        port = row[2]
        db = row[3]
        print name, ip, port, db
        driver = dbs[db](ip, int(port), driver_only=tf)
        return (name, driver)
    return None

def main(config_path):
    """
    Parameter:
     - config_path: where config file locates
    """
    service = imp.CoordImp()
    heartbeatService = himp.HeartImp()
    driver = None
    p = None
    with open(config_path) as f:
        p = int(f.readline())
        l = f.readline()
        assert l != None
        d = parseLine(l, False)
        driver = d[1]
        driver.aquireDriver(d)
        for line in f:
            driver.aquireDriver(parseLine(line, True))
    service.getDriver(driver)
    heartbeatService.getDriver(driver)
    daemon = server.RpcServer(port=p)
    daemon.registerService(service)
    daemon.registerService(heartbeatService)
    daemon.run()

if __name__ == "__main__":
    config_path = "config"
    if len(sys.argv) >= 2:
        config_path = sys.argv[1]
    main(config_path)

