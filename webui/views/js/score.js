parseDate = d3.time.format("%Y %m %d %I %M %S").parse;
var score_x, score_y, score_xx, score_xAxis, score_yAxis,
    score_yy, position = 0, text_height = 30, maxscore = null,
    score_color = d3.scale.category20();
var get_score_svg = function() {
    var margin = {top: 60, right: 80, bottom: 30, left: 50};
    var height = 500 - margin.top - margin.bottom;
    var width = 960 - margin.left - margin.right;
    score_x = d3.time.scale().range([0, width]);
    score_y = d3.scale.linear().range([height, 0]);
    score_xAxis = d3.svg.axis().scale(score_x).orient('bottom');
    score_yAxis = d3.svg.axis().scale(score_y).orient('left');
    var svg = d3.select("body").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + ',' + margin.top + ')');
    svg.append("text").attr("x", (width/2)).attr("y", 0-(margin.top/2)).attr("text-anchor", "middle")
        .style("font-size", "20px").style("text-decoration", "underline")
        .text("Throughput Graph");
    score_xx = svg.append("g").attr('class', 'x axis').attr('transform', 'translate(0,' + height + ')').call(score_xAxis);
    score_yy = svg.append("g").attr('class', 'y axis').call(score_yAxis)
    score_yy.append('text').attr("transform", "rotate(-90)").attr("y", 6).attr("dy", "0.71em")
        .style("text-anchor", "end").text("10-OPS (10 Operations per second)");
    return svg;
}
var score_svg = get_score_svg();

