var getNetStatus = function(path) {
    var margin = {top: 60, right: 80, bottom: 30, left: 100};
    var height = 500 - margin.top - margin.bottom;
    var width = 960 - margin.left - margin.right;
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
    var xAxis = d3.svg.axis().scale(x).orient('bottom');
    var yAxis = d3.svg.axis().scale(y).orient('left');
    var color = d3.scale.category20();
    var area = d3.svg.area().x(function(d) {return x(d.time);}).y0(height).y1(function(d) {return y(d.ilatency);})
        
    var svg = d3.select("body").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + ',' + margin.top + ')');
    svg.append("text").attr("x", (width/2)).attr("y", 0-(margin.top/2)).attr("text-anchor", "middle")
        .style("font-size", "20px").style("text-decoration", "underline")
        .text(path.slice(0, path.length-4) + ": Network I/O Speed Graph");
    var xx = svg.append("g").attr('class', 'x axis').attr('transform', 'translate(0,' + height + ')').call(xAxis);
    var yy = svg.append("g").attr('class', 'y axis').call(yAxis)
    yy.append('text').attr("transform", "rotate(-90)").attr("y", 6).attr("dy", ".71em")
        .style("text-anchor", "end").text("KBytes");

    var _r = svg.append("path").attr("class", "line").attr("id", "read").attr("stroke", function(d) {return color(0);});
    var _w = svg.append("path").attr("class", "line").attr('id', 'write').attr("stroke", function(d) {return color(1);});
    svg.append("text").style("fill", color(0)).text("▣ Read")
            .attr("transform", "translate(" + (width+6) + "," + (height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(1)).text("▣ Write")
            .attr("transform", "translate(" + (width+6) + "," + (2 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    
    var maxtime = null;
    var mintime = null;
    var maxspeed = null;
    var updateLatency = function() {
        var client = new XMLHttpRequest();
        client.open('GET', 'log/' + path);
        client.onreadystatechange = function() {
            if (client.readyState == 4 && client.status == 200) {
                var rows = d3.csv.parseRows(client.responseText);
                //rows = rows.slice(rows.length-100 > 0? rows.length-1200: 0, rows.length);
                var data = [];
                rows.forEach(function(d) {
                    val = {
                        time: parseDate(d[0]),
                        read: +d[1] / 1000,
                        write: +d[2] / 1000,
                    };
                    data.push(val);
                    
                    k = Math.max(val.read, val.write);
                    if (maxspeed == null || maxspeed < k)
                        maxspeed = k;
                    delete val;
                });
                if (data.length >= 1) {
                    mintime = data[0].time;
                    maxtime = data[data.length-1].time;
                }

                read = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.read);});
                write = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.write);});

                x.domain(d3.extent([mintime, maxtime]));
                y.domain([0, maxspeed]);
                xx.call(xAxis);
                yy.call(yAxis);
                _r.datum(data).attr("d", read);
                _w.datum(data).attr("d", write);
            }
        };
        client.send();
    };
    updateLatency();
    setInterval(updateLatency, 10000);   
}

