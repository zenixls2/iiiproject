var getlatency = function(path) {
    var margin = {top: 60, right: 80, bottom: 30, left: 50};
    var height = 500 - margin.top - margin.bottom;
    var width = 960 - margin.left - margin.right;
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
    var xAxis = d3.svg.axis().scale(x).orient('bottom');
    var yAxis = d3.svg.axis().scale(y).orient('left');
    var color = d3.scale.category20();
        
    var svg = d3.select("body").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + ',' + margin.top + ')');
    svg.append("text").attr("x", (width/2)).attr("y", 0-(margin.top/2)).attr("text-anchor", "middle")
        .style("font-size", "20px").style("text-decoration", "underline")
        .text(path.slice(0, path.length-4) + ": Operation Latency Graph");
    var xx = svg.append("g").attr('class', 'x axis').attr('transform', 'translate(0,' + height + ')').call(xAxis);
    var yy = svg.append("g").attr('class', 'y axis').call(yAxis)
    yy.append('text').attr("transform", "rotate(-90)").attr("y", 6).attr("dy", ".71em")
        .style("text-anchor", "end").text("latency (micro-second)");

    var _r = svg.append("path").attr("class", "line").attr("id", "read").attr("stroke", function(d) {return color(0);});
    var _i = svg.append("path").attr("class", "line").attr('id', 'insert').attr("stroke", function(d) {return color(1);});
    var _u = svg.append("path").attr("class", "line").attr('id', 'update').attr("stroke", function(d) {return color(2);});
    var _s = svg.append("path").attr("class", "line").attr('id', 'scan').attr("stroke", function(d) {return color(3);});
    var _d = svg.append("path").attr("class", "line").attr('id', 'delete').attr("stroke", function(d) {return color(4);});
    var _score = score_svg.append("path").attr("class", "line").attr("stroke", function(d) {return score_color(path);});
    svg.append("text").style("fill", color(0)).text("▣ Read")
            .attr("transform", "translate(" + (width+6) + "," + (height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(1)).text("▣ Insert")
            .attr("transform", "translate(" + (width+6) + "," + (2 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(2)).text("▣ Update")
            .attr("transform", "translate(" + (width+6) + "," + (3 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(3)).text("▣ Scan")
            .attr("transform", "translate(" + (width+6) + "," + (4 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(4)).text("▣ Delete")
            .attr("transform", "translate(" + (width+6) + "," + (5 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    score_svg.append("text").style("fill", score_color(path)).text("▣  "+ path.slice(0, path.length-4))
            .attr("transform", "translate(" + (width+6) + ',' + (position) + ')')
            .attr("dy", ".35em").attr("text-anchor", "start");
    position += text_height;
    var maxtime = null;
    var mintime = null;
    var maxlat = null;
    var updateLatency = function() {
        var client = new XMLHttpRequest();
        client.open('GET', 'log/' + path);
        client.onreadystatechange = function() {
            if (client.readyState == 4 && client.status == 200) {
                var rows = d3.csv.parseRows(client.responseText);
                //rows = rows.slice(rows.length-100 > 0? rows.length-1200: 0, rows.length);
                var data = [];
                var scores = [];
                rows.forEach(function(d) {
                    val = {
                        time: parseDate(d[0]),
                        rlatency: +d[1],
                        ilatency: +d[2],
                        dlatency: +d[3],
                        slatency: +d[4],
                        ulatency: +d[5]
                    };
                    data.push(val);
                    scores.push([val.time, +d[6] / 10]);
                    k = Math.max(val.ilatency, val.rlatency, val.ulatency, val.slatency, val.dlatency);
                    if (maxlat == null || maxlat < k)
                        maxlat = k;
                    if (maxscore == null || maxscore < +d[6] / 10)
                        maxscore = +d[6] / 10;
                    delete val;
                });
                if (data.length >= 1) {
                    maxtime = data[data.length-1].time;
                    mintime = data[0].time;
                }

                read = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.rlatency);});
                insert = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.ilatency);});
                update = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.ulatency);});
                scan = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.slatency);});
                del = d3.svg.line().x(function(d) {return x(d.time);}).y(function(d) {return y(d.dlatency);});
                score = d3.svg.line().x(function(d) {return score_x(d[0]);}).y(function(d) {return score_y(d[1]);});

                x.domain(d3.extent([mintime, maxtime]));
                y.domain([0, maxlat]);

                score_x.domain(d3.extent([mintime, maxtime]));
                score_y.domain([0, maxscore]);

                xx.call(xAxis);
                yy.call(yAxis);
                score_xx.call(score_xAxis);
                score_yy.call(score_yAxis);
                _r.datum(data).attr("d", read);
                _i.datum(data).attr("d", insert);
                _u.datum(data).attr("d", update);
                _s.datum(data).attr("d", scan);
                _d.datum(data).attr("d", del);
                _score.datum(scores).attr("d", score);
            }
        };
        client.send();
    };
    updateLatency();
    setInterval(updateLatency, 5000);   
}

