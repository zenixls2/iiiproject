
parseDate = d3.time.format("%Y %m %d %I %M %S").parse;
// open from path to get CPU status log and performs a graph
var getCPUStatus = function(path) {
    var margin = {top: 60, right: 80, bottom: 30, left: 50};
    var height = 500 - margin.top - margin.bottom;
    var width = 960 - margin.left - margin.right;
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
    var xAxis = d3.svg.axis().scale(x).orient('bottom');
    var yAxis = d3.svg.axis().scale(y).orient('left');
    var color = d3.scale.category20();
    color(4), color(3), color(2), color(1), color(0);
    var area = d3.svg.area().x(function(d) {return x(d.time);}).y0(height).y1(function(d) {return y(d.ilatency);})
        
    var svg = d3.select("body").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + ',' + margin.top + ')');
    svg.append("text").attr("x", (width/2)).attr("y", 0-(margin.top/2)).attr("text-anchor", "middle")
        .style("font-size", "20px").style("text-decoration", "underline")
        .text(path.slice(0, path.length-4) + ": Utilization Graph");
    var xx = svg.append("g").attr('class', 'x axis').attr('transform', 'translate(0,' + height + ')').call(xAxis);
    var yy = svg.append("g").attr('class', 'y axis').call(yAxis)
    yy.append('text').attr("transform", "rotate(-90)").attr("y", 6).attr("dy", ".71em")
        .style("text-anchor", "end").text("percent");

    var _n = svg.append("path").attr("class", "area").attr("id", "User").attr("fill", color(1)).attr("stroke", color(1));
    var _s = svg.append("path").attr("class", "area").attr('id', 'System').attr("fill", color(2)).attr("stroke", color(2));
    var _i = svg.append("path").attr("class", "area").attr('id', 'Idle').attr('fill', color(3)).attr("stroke", color(3));
    var _w = svg.append("path").attr("class", "area").attr('id', 'IOWait').attr('fill', color(4)).attr("stroke", color(4));
    svg.append("text").style("fill", color(1)).text("▣ User")
            .attr("transform", "translate(" + (width+6) + "," + (height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(2)).text("▣ System")
            .attr("transform", "translate(" + (width+6) + "," + (2 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(3)).text("▣ Idle")
            .attr("transform", "translate(" + (width+6) + "," + (3 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    svg.append("text").style("fill", color(4)).text("▣ IOWait")
            .attr("transform", "translate(" + (width+6) + "," + (4 * height/5) + ")")
            .attr("dy", ".35em").attr("text-anchor", "start");
    var maxtime = null;
    var mintime = null;
    var updateLatency = function() {
        var client = new XMLHttpRequest();
        client.open('GET', 'log/' + path);
        client.onreadystatechange = function() {
            if (client.readyState == 4 && client.status == 200) {
                var rows = d3.csv.parseRows(client.responseText);
                //rows = rows.slice(rows.length-100 > 0? rows.length-1200: 0, rows.length);
                var data = [];
                rows.forEach(function(d) {
                    val = {
                        time: parseDate(d[0]),
                        user: +d[1],
                        system: +d[2],
                        idle: +d[3],
                        iowait: +d[4]
                    };
                    val.system = val.user + val.system;
                    val.idle = val.idle + val.system;
                    val.iowait = val.iowait + val.idle;
                    data.push(val);
                    delete val;
                });
                if (data.length >= 1) {
                    mintime = data[0].time;
                    maxtime = data[data.length-1].time;
                }


                user = d3.svg.area().x(function(d) {return x(d.time);})
                        .y0(y(0))
                        .y1(function(d) {
                            if (d.iowait != 0)
                                return y(d.user/d.iowait);
                            return y(0);});
                system = d3.svg.area().x(function(d) {return x(d.time);})
                        .y0(function(d) {
                            if (d.iowait != 0)
                                return y(d.user/d.iowait);
                            return y(0);})
                        .y1(function(d) {
                            if (d.iowait != 0)
                                return y(d.system/d.iowait);
                            return y(0);});
                idle = d3.svg.area().x(function(d) {return x(d.time);})
                        .y0(function(d) {
                            if (d.iowait != 0)
                                return y(d.system/d.iowait);
                            return y(0);})
                        .y1(function(d) {
                            if (d.iowait != 0)
                                return y(d.idle/d.iowait);
                            return y(0)});
                iowait = d3.svg.area().x(function(d) {return x(d.time);})
                        .y0(function(d) {
                            if (d.iowait != 0)
                                return y(d.idle/d.iowait);
                            return y(0);})
                        .y1(y(1));

                x.domain(d3.extent([mintime, maxtime]));
                y.domain([0, 1]);
                xx.call(xAxis);
                yy.call(yAxis);
                _n.datum(data).attr("d", user);
                _s.datum(data).attr("d", system);
                _i.datum(data).attr("d", idle);
                _w.datum(data).attr("d", iowait);

            }
        };
        client.send();
    };
    updateLatency();
    setInterval(updateLatency, 5000);   
}

