cdef class Callable(object):
    cdef public object n
    cpdef run(self, object n)
