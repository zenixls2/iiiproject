import socketrpc.rpc_pb2 as rpc_pb
from cpython cimport bool
from socketrpc.controller import RpcController
import socket
import select
import logging
from libc.stdlib cimport strtol
cimport systime

cdef class datatuple:
    cdef int length
    cdef bytes msg
    def __cinit__(self):
        self.length = 0
        self.msg = bytes('')

cdef class Callable(object):
    def __cinit__(self):
        self.n = None
    cpdef run(self, object n):
        self.n = n

cdef class RpcServer(object):
    cdef int _buffer_len
    cdef bool running
    cdef object sock
    cdef dict serviceMap
    def __cinit__(self, bytes host=b'', int port=8090):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((host, port))
        self.sock.listen(5)
        self.running = True
        self._buffer_len = 8192
        self.serviceMap = {}

    def run(self):
        cdef bytes num
        cdef bytes toSend
        cdef list inlist = [self.sock]
        cdef datatuple t
        cdef dict datamap = {}
        while self.running:
            systime._sleep(1000)
            inputready, outputready, exceptready = select.select(inlist, [], [])
            for s in inputready:
                if s == self.sock:
                    client, address = self.sock.accept()
                    inlist.append(client)
                    datamap[client] = datatuple()
                else:
                    try:
                        t = datamap[s]
                        if t.length == 0:
                            num = s.recv(6)
                            if num:
                                t.length = int(num, 16)
                                #t.length = strtol(num, NULL, 16)
                        elif t.length > self._buffer_len:
                            t.msg += s.recv(self._buffer_len)
                            t.length -= self._buffer_len
                        else:
                            t.msg += s.recv(t.length)
                            t.length = 0
                            toSend = self.handler(t.msg)
                            s.sendall("%06x%s" % (len(toSend), toSend))
                            t.msg = b''
                    except socket.error, msg:
                        logging.error(msg)
                        del datamap[s]
                        inlist.remove(s)
                        s.close()
                    except ValueError:
                        print "num not base 16"
                        del datamap[s]
                        s.close()
                        inlist.remove(s)
        for s in inlist:
            s.close()

    cdef handler(self, bytes buf):
        cdef object request = rpc_pb.Request()
        request.MergeFromString(buf)
        cdef list method_lists = self.serviceMap[request.service_name]
        cdef object service = method_lists[0]
        cdef object method = method_lists[1][request.method_index][1]
        cdef object proto_request = method_lists[1][request.method_index][0]()
        proto_request.MergeFromString(request.request_proto)
        cdef int t = systime.clock()
        cdef object controller = RpcController()
        cdef Callable runner = Callable()
        service.CallMethod(method, controller, proto_request, runner)
        cdef object rpcResponse = rpc_pb.Response()
        if runner.n:
            rpcResponse.response_proto = runner.n.SerializeToString()
        return rpcResponse.SerializeToString()

    def registerService(self, service):
        self.serviceMap[service.GetDescriptor().full_name] = [service,
            map(lambda y: [service.GetRequestClass(y), y], sorted(service.DESCRIPTOR.methods, key=lambda x: x.index))]
