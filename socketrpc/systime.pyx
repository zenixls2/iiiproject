cdef extern from 'unistd.h':
    int usleep(unsigned int s)
cdef extern from 'time.h':
    struct timespec:
        int tv_sec
        int tv_nsec
    int clock_gettime(int id, timespec* t)
    int CLOCK_MONOTONIC

cdef void _sleep(unsigned int s):
    usleep(s)

cdef int clock():
    cdef timespec t
    clock_gettime(CLOCK_MONOTONIC, &t)
    return t.tv_sec * 1000000 + t.tv_nsec / 1000
