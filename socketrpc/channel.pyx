import socketrpc.rpc_pb2 as rpc
import google.protobuf.service as service
import socket
import logging
from libc.stdlib cimport strtol
cimport systime

cdef class RpcChannel(object):
    cdef int _buffer_len
    cdef str host
    cdef int port
    cdef object sock
    def __cinit__(self, str host, int port):
        self._buffer_len = 8192
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((host, port))
        except (socket.gaierror, socket.error), msg:
            logging.error(msg)
            self.sock.close()
            raise Exception("Connection fail")

    def reconnect(self):
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((self.host, self.port))
        except (socket.gaierror, socket.error), msg:
            logging.error(msg)
            self.sock.close()
            raise Exception("Connection Fail")

    def CallMethod(self, method, controller, request, response_class, done=None):
        cdef bytes toSend
        cdef int length = 0
        cdef bytes data = b''
        cdef bytes num
        
        serviceResponse = None
        rpcRequest = rpc.Request()
        rpcRequest.request_proto = request.SerializeToString()
        rpcRequest.service_name = method.containing_service.full_name
        rpcRequest.method_index = method.index

        toSend = rpcRequest.SerializeToString()
        while True:
            try:
                self.sock.sendall('%06x%s' % (len(toSend), toSend))
                num = self.sock.recv(6)
                if num:
                    length = strtol(num, NULL, 16)
                while True:
                    if length > self._buffer_len:
                        length -= self._buffer_len
                        data += self.sock.recv(self._buffer_len)
                    else:
                        data += self.sock.recv(length)
                        break
                break
            except socket.error as msg:
                data = b''
                logging.error(msg)
                systime._sleep(1000)
                self.reconnect()
        
        rpcResponse = rpc.Response()
        rpcResponse.ParseFromString(data)
        if rpcResponse.HasField('response_proto'):
            serviceResponse = response_class()
            serviceResponse.ParseFromString(rpcResponse.response_proto)
        
        # If blocking, return response
        if done is None:
            return serviceResponse
        done.run(serviceResponse)
