from __future__ import print_function
import sys
def log(*objs):
    print("__logging__: ", *objs, file=sys.stderr)

def error(*objs):
    print("___error___: ", *objs, file=sys.stderr)
