import sys
from dbtypes import *
import types
import time

print("unit: millisecond")

def operation(toprint, callee, *args):
    r = None
    t = time.time()
    r = callee(*args)
    if toprint:
        print(str(int(t * 1000000)).ljust(8) + "|" + 
          str(int((time.time() - t) * 1000000)).ljust(6) + " " + 
          callee.__name__.ljust(7) + ": " + str(r))
    return r

def validate(msg, to_valid, valid_val):
    print("\t" + msg + ": " + str(to_valid == valid_val))

table = "test"

print("MongoDB Scan Test")
b = MongoDriver(host="127.0.0.1", port=30000)
b.drop(table)
for i in xrange(500):
    b.insert(table, str(i), {"1": "1", "2" : "2"})
r = b.scan(table, "400", 50, ["1", "2"])
validate("scan test", r["400"], {"1": "1", "2": "2"})
b.drop(table)
b.disconnect()

print("Redis Scan Test")
c = RedisDriver('localhost', 6379)
c.drop(table)
for i in xrange(500):
    c.insert(table, str(i), {"1": "1", "2": "2"})
r = c.scan(table, "400", 50, ["1", "2"])
validate("scan test", r["400"], {"1": "1", "2": "2"})
c.drop(table)
c.disconnect()

print("Hbase Scan Test")
d = HbaseDriver('localhost', 9090)
d.drop(table)
for i in xrange(500):
    d.insert(table, str(i), {"1": "1", "2": "2"})
r = d.scan(table, "400", 50, ["1", "2"])
validate("scan test", r["400"], {"1": "1", "2": "2"})
d.drop(table)
d.disconnect()


a = AttilaDriver()

a.drop(table)
class Product:
    price = 0
    name = ""
    stock = 0
    def __init__(self, p, n, s):
        self.price = p
        self.name = n
        self.stock = s

    def hash(self, k):
        return str(ord(k[-1]))

    def encode(self):
        return table, self.hash(self.name), {"price": str(self.price), "stock": str(self.stock)}

p = Product(200, "PC", 10000)
a.insert(*p.encode())
p = Product(300, "laptop", 1000)
a.insert(*p.encode())
print a.read(table, p.hash("PC"), ["price", "stock"], True)
print a.read(table, p.hash("laptop"), ["price", "stock"], True)
print 0, a.listkeys(0, table)
print 1, a.listkeys(1, table)
print 2, a.listkeys(2, table)
a.migrate(1, 0, 1)
print 0, a.listkeys(0, table)
print 1, a.listkeys(1, table)
print 2, a.listkeys(2, table)
a.migrate(1, 1, 2)
print 0, a.listkeys(0, table)
print 1, a.listkeys(1, table)
print 2, a.listkeys(2, table)
#a.drop(table)
a.disconnect()
exit()

print("Insertion Test")
a.drop(table)
for i in xrange(500):
    a.insert(table, str(i), {"1": str(i), "2": str(i+1)})
total = len(a.listkeys(0, table)) + len(a.listkeys(1, table))
validate("total inserted", total, 500)
a.drop(table)

print("Read Test")
a.drop(table)
for i in xrange(10):
    a.insert(table, str(i), {"1": str(i), "2": str(i+1)})
    r = a.read(table, str(i), ["1", "2"] ,False)
    validate("read from key " + str(i), r, [str(i), str(i+1)])
a.drop(table)

print("Update Test")
a.drop(table)
for i in xrange(10):
    a.insert(table, str(i), {"1": "", "2": ""})
    a.update(table, str(i), {"1": str(i), "2": str(i+1)})
    validate("update from key " + str(i), a.read(table, str(i), ["1", "2"], False), [str(i), str(i+1)])
a.drop(table)

print("Migration Test")
a.drop(table)
for i in xrange(500):
    a.insert(table, str(i), {"1": "1", "2": "2"})
keys0 = a.listkeys(0, table)
keys1 = a.listkeys(1, table)
keys2 = a.listkeys(2, table)
a.migrate(1, 0, 1)
a.migrate(1, 0, 2)
keys0_n = a.listkeys(0, table)
keys1_n = a.listkeys(1, table)
keys2_n = a.listkeys(2, table)
validate("migration from 0 to 1, 0 to 2", len(keys0) - len(keys0_n), 200)
validate("migration from 0 to 1, 0 to 2", len(keys1_n) - len(keys1), 100)
validate("migration from 0 to 1, 0 to 2", len(keys2_n) - len(keys2), 100)
a.drop(table)

print("Scan Test")
a.drop(table)
for i in xrange(500):
    a.insert(table, str(i), {"1": "1", "2": "2"})
r = a.scan(table, "400", 50, ["1", "2"])
validate("scan from key=400, num=50", len(r), 50)
validate("scan from key=400, num=50", r["400"], {"1": "1", "2": "2"})
a.drop(table)

print("Scan Multiple Test")
a.drop(table)
for i in xrange(500):
    a.insert(table, str(i), {"1": "1", "2": "2"})
a.migrate(1, 0, 1)
a.migrate(1, 0, 2)
r = a.scan(table, "0", 300, ["1", "2"])
validate("scan across node 2 {100-199} node 1 {0-99} and node 0 {200-299}", len(r), 300)
a.drop(table)

a.disconnect()

#print("Test")
#operation(True, a.drop, table)
#operation(True, a.delete, table, "1")
#operation(True, a.insert, table, "1", {"1": "222", "2": "333"})
#operation(True, a.read, table, "1", ["1"], True)
#operation(True, a.update, table, "1", {"1": "333"})
#operation(True, a.read, table, "1", ["1"], True)
#operation(True, a.getkeys, table, "")
#operation(True, a.read, table, "1", None, True)
#operation(True, a.listkeys, 0, table)
#operation(True, a.listkeys, 1, table)
#num, target, tid
#operation(True, a.migrate, 1, 0, 1)
#operation(True, a.listkeys, 0, table)
#operation(True, a.listkeys, 1, table)
#operation(True, a.insert, table, "1111", {"2": "222"})
#operation(True, a.insert, table, "11", {"1": "222"})
#operation(True, a.getkeys, table, "")
#operation(True, a.scan, table, "11", 3, ["1", "2"])
#operation(True, a.delete, table, "1")
#operation(True, a.getstatus, 0)
#operation(True, a.drop, table)
#a.disconnect()

