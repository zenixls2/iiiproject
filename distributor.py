#!/usr/bin/env python
import sys, time, os
import dbtypes
import socket, select
import thread
import hc
import msgpack
from multiprocessing import Process
from operations import read, insert, update, scan, delete, migrate, getkeys, drop, listkeys, init_groups, getstatus, NodeInfo

def dispatcher(sock, node_counters):
    """
    worker process
    Paramter:
     - sock: socket to listen from
     - node_counters: NodeInfo
    """
    ops = [read, insert, update, scan, delete, migrate, getkeys, drop, listkeys, getstatus]
    for node in node_counters:
        node.getdriver()
    while True:
        try:
            num = sock.recv(6)
            if num:
                length = int(num, 16)
                got = length
                msg = sock.recv(length)
                got -= len(msg)
                while got > 0:
                    recv = sock.recv(length)
                    got -= len(recv)
                    msg += recv
                req = msgpack.unpackb(msg)
                ret = ops[req[0]](node_counters, req)
                toSend = msgpack.packb(ret)
                sock.sendall("%06x%s" % (len(toSend), toSend))
            else:
                break
        except socket.error as msg:
            if msg.errno == 11:
                print msg
                pass
            else:
                print "Transfer fail"
                sock.close()
                break
        except ValueError:
            print "Value not base 16"
            sock.close()
            break
    return


def main(config, server_port=10000):
    """
    Parameter:
     - config: config path (str)
     - server_port: coordinator port (int)
    """
    node_names = {}
    node_counters = []
    dispatchers = []

    init_groups()
    
    # read config file
    f = open(config, 'r')
    lines = f.readlines()
    f.close()

    counter = 0
    for i in lines:
        args = i[0:-1].split()
        name = args[0]
        ip = args[1]
        rpc_port = int(args[2])
        driver_port = int(args[3])
        driver_type = args[4]
        operable = args[5]
        node_counters.append(NodeInfo(name, counter, ip, rpc_port, driver_type, driver_port, operable == "master"))
        node_names[name] = node_counters[-1]
        counter += 1

    # intit server socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', server_port))
    sock.listen(5)
    
    # start heartbeat collector
    p = Process(target=hc.heartbeatCollector, args=(node_counters, node_names))
    p.daemon = True
    p.start()

    # accept to start new daemon
    while True:
        inlist, _, _ = select.select([sock], [], [])
        for rsock in inlist:
            if rsock == sock:
                client, address = rsock.accept()
                p = Process(target=dispatcher, args=(client, node_counters))
                p.daemon = True
                dispatchers.append(p)
                p.start()
                client.close()
            time.sleep(0.1)

if __name__ == "__main__":
    main("config", 10000)
