#!/usr/bin/env python
# -*- coding: utf-8 -*-
import coordinator.heartbeat_pb2 as heart
import time
import os
import re
import resource
import msgpack
from multiprocessing import cpu_count
import operations
class Network(object):
    timestamp = None
    read = None
    write = None

class Disk(object):
    read = None
    write = None
    timestamp = None
    rtime = None
    wtime = None
    def __init__(self, r, w, t, wt, rt):
        self.read = r
        self.write = w
        self.timestamp = t
        self.wtime = wt
        self.rtime = rt

class database(object):
    @staticmethod
    def init_decorator(func):
        def __init__(self, host, port, passwd=None, logpath=None, configpath=None, driver_only=False):
            self.status = None
            self.pids = None
            self.pagesize = resource.getpagesize()
            self.last_network = Network()
            self.last_disks = {}
            self.cpus = {}
            self.last_cpus = {}
            self._path = {}
            self.nodes = {}
            paths = open('/proc/mounts', 'rt')
            lines = paths.readlines()
            paths.close()
            for line in lines:
                tp, pth = line.split()[:2]
                if tp.startswith("/dev"):
                    if tp.startswith("/dev/disk/by-uuid/"):
                        tp = "/dev/" + os.readlink(tp)[6:]
                    self._path[tp] = pth
            func(self, host, port, passwd, logpath, configpath, driver_only)
            if not driver_only:
                self.monitor()
        return __init__

    def __init__(self, host, port, passwd=None, logpath=None, configpath=None, driver_only=False):
        pass

    def __del__(self):
        self.disconnect()

    def aquireDriver(self, node):
        """
        Parameter:
         - node: (db_name, driver_instance) (tuple)
        """
        if node:
            self.nodes[node[0]] = node[1]

    def read(self, table, key, fields, withfields):
        pass

    def delete(self, table, key):
        pass

    def insert(self, table, key, values):
        pass

    def scan(self, table, startkey, recordcount, fields):
        pass

    def update(self, table, key, values):
        pass

    def getkeys(self, table, hashkey):
        pass

    def migrate(self, tk, target):
        """
        @return - keys to remove from origin
        Parameter:
         - tk: {table:[keys...], ...}
         - target: db name to aquire data from
        """
        driver = self.nodes[target]
        result = {}
        for table, l in tk.iteritems():
            result[table] = []
            for k in l:
                for key in operations.iter_group(k):
                    val = driver.read(table, key, fields=None, withfields=True)
                    if val:
                        self.insert(table, key, val)
                        result[table].append(key)
        return result


    #def merge(self, num, target, tid, interval):
    #    driver = self.nodes[target]
    #    cache = self.nodes['cache']
    #    tk = cache.getlastfreq(num, tid, interval)
    #    if tk:
    #        print(driver, num, len(tk))
    #        for table_hash, keys in tk.items():
    #            # TODO: aquire write lock
    #            table, key = table_hash.split('_')
    #            for k in keys:
    #                val = driver.read(table, key + k, withfields=True)
    #                self.insert(table, key + k, val)
    #                cache.setprio(table, key, tid)
            # return for removal
    #        return msgpack.packb(tk.keys())
    #    else:
            # []
     #       print "merge transfer fault", target, num, tid, interval
     #       return '\x90'
    
    #def merge(self, table, hashkey, fields, target, prio):
    #    driver = self.nodes[target]
    #    keys = driver.getkeys(table, hashkey)
    #    print(hashkey, target, keys)
    #    for k in keys:
    #        val = driver.read(table, k, fields)
    #        self.insert(table, k, dict(zip(fields, val)))
        # self.nodes['cache'].setprio(table, hashkey, prio)
    #    return
        # notify to untag taret for hashkey
        # driver.deleteall(hashkey)
    
    def disconnect(self):
        pass
    
    def shutdown(self):
        pass
    
    def _monitor_disk(self, s):
        """
        get disk status and push into HeartbeatResponse s
        Parameter:
         - s: (HeartbeatResponse)
        """
        SECTOR_SIZE = 512
        f = open('/proc/diskstats', 'rt')
        lines = f.readlines()
        f.close()
        t = time.time()
        for line in lines:
            _, _, name, _, _, rbytes, rtime, _, _, wbytes, wtime = line.split()[:11]
            if self._path.has_key('/dev/' + name):
                dev = os.statvfs(self._path['/dev/' + name])
                rbytes = int(rbytes) * SECTOR_SIZE
                wbytes = int(wbytes) * SECTOR_SIZE
                rtime = int(rtime)
                wtime = int(wtime)
                if self.last_disks.has_key(name):
                    d = self.last_disks[name]
                    hd = s.disks.add()
                    hd.name = name
                    hd.read_speed = int((rbytes - d.read) / (t - d.timestamp))
                    #hd.read_speed = int((rbytes - d.read) / (rtime - d.rtime))
                    hd.write_speed = int((wbytes - d.write) / (t - d.timestamp))
                    #hd.write_speed = int((wbytes - d.write) / (wtime - d.wtime))
                    hd.total_blocks = dev.f_blocks
                    hd.avail_blocks = dev.f_bavail
                    hd.block_size = dev.f_bsize
                    del self.last_disks[name]
                self.last_disks[name] = Disk(rbytes, wbytes, t, rtime, wtime)

    def _monitor_cpu(self, status):
        """
        get cpu status and push into HeartbeatResponse s
        Parameter:
         - status: (HeartbeatResponse)
        """

        f = open("/proc/stat", "r")
        lines = f.read().splitlines(False)[:cpu_count()+1]
        f.close()
        if not self.last_cpus:
            for l in lines:
                name, user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice = l.split()
                self.last_cpus[name] = [int(user), int(system), int(idle), int(iowait)]
        else:
            for l in lines:
                name, user, nice, system, idle, iowait, _, _, _, _, _ = l.split()
                last = self.last_cpus[name]
                self.cpus[name] = [int(user) - last[0], int(system) - last[1], int(idle) - last[2], int(iowait) - last[3]]
                last[0] = int(user)
                last[1] = int(system)
                last[2] = int(idle)
                last[3] = int(iowait)
                c = status.cpus.add()
                c.user = self.cpus[name][0]
                c.system = self.cpus[name][1]
                c.idle = self.cpus[name][2]
                c.iowait = self.cpus[name][3]


    def _monitor_network(self, status):
        """
        get network status and push into HeartbeatResponse s
        Parameter:
         - status: (HeartbeatResponse)
        """
        tmptime = time.time()
        f = open("/proc/net/dev")
        lines = f.read().splitlines(False)[2:]
        f.close()
        nbytesin = 0
        nbytesout = 0
        for line in lines:
            name, rbytes, _, _, _, _, _, _, _, tbytes = line.split()[:10]
            nbytesin += int(rbytes)
            nbytesout += int(tbytes)
        if self.last_network.timestamp:
            status.network.read_speed = \
                int((nbytesin - self.last_network.read) / (tmptime - self.last_network.timestamp))
            status.network.write_speed = \
                int((nbytesout - self.last_network.write) / (tmptime - self.last_network.timestamp))
            self.last_network.read = nbytesin
            self.last_network.write = nbytesout
            self.last_network.timestamp = tmptime
        else:
            self.last_network.read = nbytesin
            self.last_network.write = nbytesout
            self.last_network.timestamp = tmptime

    def setpids(self):
        raise Exception("setpids non-implement.")


    @staticmethod
    def monitor_decorator(func):
        def monitor(self):
            status = heart.Heartbeat()
            self._monitor_disk(status)
            self._monitor_network(status)
            self._monitor_cpu(status)
            # /proc/vmstat: swap memory
            # /proc/stat: cpu
            # /proc/{pid}/fd/*: socket, files
            # /proc/filesystems: disk partitions
            # /proc/{pid}/io: rchar - read passed by function, read_bytes - read from disk
            mem_file = open('/proc/meminfo', "r")
            splitter = re.compile(':[ ]+')
            mem = dict(list(splitter.split(x) for x in mem_file.read().splitlines(False)))
            mem_file.close()
            status.memory.total_storage = int(mem['MemTotal'][:-2]) / 1024
            try:
                status.memory.available_storage = int(mem['MemAvailable'][:-2]) / 1024
            except KeyError:
                status.memory.available_storage = (int(mem['MemFree'][:-2]) + int(mem['Buffers'][:-2]) + int(mem['Cached'][:-2])) / 1024
            self.setpids()
            total_rss = 0
            for i in self.pids:
                mem_file = open('/proc/%s/statm' % i)
                _, rsspages = mem_file.read().split()[:2]
                mem_file.close()
                total_rss += int(rsspages) * self.pagesize / 1024 / 1024
            status.memory.current_storage = total_rss
            func(self, status)
            return status
        return monitor

    def monitor(self, status):
        pass
