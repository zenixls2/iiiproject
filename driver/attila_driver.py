import msgpack
import socket, select
import logging
#from libc.stdlib cimport strtol
class AttilaDriver(object):
    def __init__(self, host="localhost", port=10000, driver_only=False):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sock.connect((host, port))
        except (socket.gaierror, socket.error), msg:
            logging.error(msg)
            self.sock.close()
            raise Exception("connection fail")
        #self.sock.setblocking(0)

    def __del__(self):
        self.disconnect()

    def _send_recieve(self, toSend):
        """
        Send toSend str to remote server with reconnection feature.
        """
        length = 0
        while True:
            try:
                self.sock.sendall("%06x%s" % (len(toSend), toSend))
                num = self.sock.recv(6)
                if num:
                    length = int(num, 16)
                    got = length
                    data = self.sock.recv(length)
                    got -= len(data)
                    while got > 0:
                        recv = self.sock.recv(got)
                        got -= len(recv)
                        data += recv
                    return data
                return None
            except socket.error as msg:
                logging.error(msg)
                print("Transfer error")
                if msg.errno == 11:
                    pass
                else:
                    time.sleep(0.05)
                    self.reconnect()

    def reconnect(self):
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sock.connect((self.host, self.port))
        except (socket.gaierror, socket.error), msg:
            logging.error(msg)
            self.sock.close()
            raise Exception("Connection Fail")

    def disconnect(self):
        self.sock.close()

    def read(self, table, key, fields, withfields=False):
        return msgpack.unpackb(self._send_recieve(msgpack.packb([0, table, key, fields, withfields])))

    def insert(self, table, key, values):
        return msgpack.unpackb(self._send_recieve(msgpack.packb([1, table, key, values])))
    
    def update(self, table, key, values):
        return msgpack.unpackb(self._send_recieve(msgpack.packb([2, table, key, values])))
    
    def scan(self, table, startkey, recordcount, fields):
        return msgpack.unpackb(self._send_recieve(msgpack.packb([3, table, startkey, recordcount, fields])))

    def delete(self, table, key):
        """
        Delete key in table.
        """
        return msgpack.unpackb(self._send_recieve(msgpack.packb([4, table, key])))
        
    def migrate(self, num, target, tid):
        """
        Migrate data from target to tid.
        @return - True if migration ok. False if any one of the nodes is not operable.
        Parameter:
         - num: total number of groups to move. (int)
         - target: the data provider node id.
         - tid: the data receiver node id.
        """
        return msgpack.unpackb(self._send_recieve(msgpack.packb([5, num, target, tid])))

    def getkeys(self, table, k):
        """
        Get keys of specific table with prefix k.
        @return - A list of keys in the clusters start with k and in the table of table.
        Parameter:
         - table: table name (str)
         - k: key prefix (str)
        """
        return msgpack.unpackb(self._send_recieve(msgpack.packb([6, table, k])))

    def drop(self, table):
        """
        Remove the table from the cluster.
        Parameter:
         - table: table name (str)
        """
        return msgpack.unpackb(self._send_recieve(msgpack.packb([7, table])))

    def listkeys(self, nid, table):
        """
        List all keys in node id = nid and table name = table.
        @return - list of keys of specific node for a single table.
        Parameter:
         - nid: node id (int)
         - table: table name (str)
        """
        return msgpack.unpackb(self._send_recieve(msgpack.packb([8, nid, table])))

    def getstatus(self, nid):
        """
        Get node nid status
        @return - status represented in str
        Parameter:
         - nid: node id (int)
        """
        return msgpack.unpackb(self._send_recieve(msgpack.packb([9, nid])))
