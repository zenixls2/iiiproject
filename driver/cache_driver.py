import msgpack
import socket, select
import time
import logging
class CacheDriver(object):
    def __init__(self, host="localhost", port=10001, driver_only=True):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sock.connect((host, port))
        except (socket.gaierror, socket.error), msg:
            logging.error(msg)
            self.sock.close()
            raise Exception("connection fail")
                    
    def __del__(self):
        self.disconnect()

    def _send_recv(self, toSend):
        length = 0
        while True:
            try:
                self.sock.sendall("%06x%s" % (len(toSend), toSend))
                num = self.sock.recv(6)
                if num:
                    length = int(num, 16)
                    got = length
                    data = self.sock.recv(length)
                    got -= len(data)
                    while got > 0:
                        recv = self.sock.recv(got)
                        data += recv
                        got -= len(recv)
                    return data
                return '\xc0'
            except socket.error as msg:
                logging.error(msg)
                print("Transfer Error")
                if msg.errno == 11:
                    pass
                else:
                    time.sleep(0.05)
                    self.reconnect()

    def reconnect(self):
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.sock.connect((self.host, self.port))
        except (socket.gaierror, socket.error), msg:
            logging.error(msg)
            self.sock.close()
            raise Exception("Connection Fail")

    def disconnect(self):
        self.sock.close()

    def add(self, table, key, value, prio, incr=1, insert=False):
        self._send_recv(msgpack.packb([0, table, key, value, prio, incr, insert]))
        return

    def remove(self, table, prio, key, value):
        self._send_recv(msgpack.packb([1, table, prio, key, value]))
        return

    def getlastfreq(self, num, target, interval):
        return msgpack.unpackb(self._send_recv(msgpack.packb([2, num, target, interval])))

    def setprio(self, table, key, val):
        self._send_recv(msgpack.packb([3, table, key, val]))
        return

    def getprio(self, table, key):
        return msgpack.unpackb(self._send_recv(msgpack.packb([4, table, key])))

    def dropattrs(self):
        self._send_recv(msgpack.packb([5]))
        return

    def dropkeys(self, table):
        self._send_recv(msgpack.packb([6, table]))
        return

    def initsortedset(self):
        self._send_recv(msgpack.packb([7]))
        return

