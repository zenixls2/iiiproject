#!/usr/bin/env/python
# -*- coding: utf-8 -*-
import coordinator.heartbeat_pb2 as heart
from db import database
import redis
import os
import msgpack

#TODO: How to map table
# current solution => tablename_key
# but may still get collision if not designed well
# suppose use hash function ?

#TODO: Sorted items are in another structure, deal with it

#TODO: lua script eval support

class RedisDriver(database):
    pool = None
    r = None
    keymap = '_keymap.'
    key = '_key.'
    sortedkey = '_sorted.'
    index_key = "_indices."
    @database.init_decorator
    def __init__(self, host='localhost', port=6379, passwd=None, logpath=None, configpath=None, driver_only=False):
        self.pool = redis.ConnectionPool(host=host, port=port)
        self.r = redis.Redis(connection_pool = self.pool)
        lua = ''' 
            local value = redis.call('KEYS', '_keymap.'..KEYS[1]..'_*')
            table.sort(value)
            local num = tonumber(ARGV[1])
            local t = {}
            local tocmp = '_keymap.'..KEYS[2]
            for _, key in ipairs(value) do
                if key >= tocmp then
                    if num > 0 then
                        local v = 0
                        if not next(t) then
                            local tmp = redis.call('lrange', key, 0, -1)
                            for _, k in ipairs(tmp) do
                                if k >= KEYS[3] then
                                    v = v + 1
                                end
                            end
                        else
                            v = redis.call('llen', key)
                        end
                        if num > v then
                            num = num - v
                            t[key] = v
                        else
                            t[key] = num
                            break
                        end
                    else
                        break
                    end
                end
            end
            return cmsgpack.pack(t)
        '''
        self.getlength = self.r.register_script(lua)
        lua = '''
            local v = redis.call('keys', ARGV[1]..'_*')
            redis.call('del', '_indices.'..ARGV[1])
            for _, k in ipairs(v) do
                redis.call('del', k)
            end
        '''
        self.droptable = self.r.register_script(lua)
        lua = '''
            local v = redis.call('zrange', '_sorted.'..ARGV[2], 0, -1, 'withscores')
            local score = tonumber(ARGV[1])
            local interval = tonumber(ARGV[3])
            local c = {}
            for i = 1, #v-1, 2 do
                local num = tonumber(v[i+1]) / interval
                if num <= score then
                    local val = redis.call('lrange', '_keymap.'..v[i], 0, -1)
                    local tmp = {v[i]}
                    table.insert(tmp, val)
                    table.insert(c, tmp)
                    score = score - num
                else
                    break
                end
            end
            return cmsgpack.pack(c)
        '''
        self.getlastkey = self.r.register_script(lua)
        lua = '''
            local v = redis.call('keys', '_sorted.*')
            for _, t in ipairs(v) do
                local v = redis.call('zrange', t, 0, -1)
                for _, key in ipairs(v) do
                    redis.call('zadd', t, 1, key)
                end
            end
        '''
        self.initsort = self.r.register_script(lua)
        lua = '''
            local v = redis.call('zrangebyscore', KEYS[1], KEYS[2], '+inf', 'LIMIT', 0, tonumber(KEYS[3]))
            local t = {}
            local l = tonumber(KEYS[4]) + 2
            for _, key in ipairs(v) do
                local c = redis.call('hmget', key, unpack(ARGV))
                local i, s = next(c, nil)
                local d = {}
                for _, k in ipairs(ARGV) do
                    d[k] = s
                    i, s = next(c, i)
                end
                t[string.sub(key, l)] = d
            end
            return cmsgpack.pack(t)
        '''
        self._scan = self.r.register_script(lua)


    def drop(self, table):
        self.droptable(args=[table])

    def hashcode(self, key):
        h = 0
        for c in key:
            h = ord(c) + h * 128
        return h
        #for c in key:
        #    h = (31 * h + ord(c)) & 0xFFFFFFFF
        #return ((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000

    
# attr related api
# currently of no use. (deprecated)
    def getlastfreq(self, num, target, interval):
        print(interval)
        return msgpack.unpackb(self.getlastkey(args=[num, target, interval]))

    def initsortedset(self):
        self.initsort()

    def dropattrs(self):
        self.r.flushdb()

    def dropkeys(self, table):
        p = self.r.pipeline(False)
        self.droptable(args=[self.keymap+table], client=p)
        self.droptable(args=[self.key+table], client=p)
        # FIXME:
        #p.delete(self.sortedkey+table)
        p.execute()
 
    def add(self, table, key, value, prio, incr=1, insert=False):
        k = key
        if table != None:
            key = table + '_' + key
        if insert:
            p = self.r.pipeline(False)
            p.rpush(self.keymap + key, value)
            p.setnx(self.key + key, prio)
            p.zadd(self.sortedkey + str(prio), key, incr)
            p.execute()
        else:
            self.r.zincrby(self.sortedkey + str(prio), key, incr)
    
    #def getscore(self, table, key, prio):
    #    if table != None:
    #        key = table + '_' + key
    #    return self.r.zscore(self.sortedkey + str(prio), key)

    #def get(self, table, key):
    #    if table != None:
    #        key = table + '_' + key
    #    return self.r.lrange(self.keymap + key, 0, -1)

    def getrangelength(self, table, key, endkey, num):
        if table != None:
            key = table + '_' + key
        return msgpack.unpackb(self.getlength(keys=[table, key, endkey], args=[num]))

    def getprio(self, table, key):
        if table != None:
            key = table + '_' + key
        ret = self.r.get(self.key + key)
        if ret == "None":
            return None
        return ret

    def setprio(self, table, key, val):
        if table != None:
            key = table + '_' + key
        prio = self.r.get(self.key + key)
        #score = self.r.zscore(self.sortedkey + str(prio), key)
        p = self.r.pipeline(False)
        p.set(self.key + key, val)
        p.zrem(self.sortedkey + str(prio), key)
        p.zadd(self.sortedkey + str(val), key, 1)
        p.execute()

    def remove(self, table, prio, key, value = None):
        if table != None:
            key = table + '_' + key
        if value:
            self.r.lrem(self.keymap + key, value, 0)
        else:
            p = self.r.pipeline(False)
            p.delete(self.keymap + key)
            p.delete(self.key + key)
            p.zrem(self.sortedkey + str(prio), key)
            p.execute()

    def length(self, table, key):
        if table != None:
            key = table + '_' + key
        return self.r.llen(self.keymap + key)

# end of attr related api

    def read(self, table, key, fields=None, withfields=False):
        if table != None:
            key = table + "_" + key
        if fields == None:
            if withfields:
                return self.r.hgetall(key) 
            return self.r.hgetall(key).values()
        else:
            if withfields:
                return dict(zip(fields, self.r.hmget(key, fields)))
            return self.r.hmget(key, fields)

    # values: map
    def insert(self, table, key, values):
        if table != None:
            k = key
            key = table + '_' + key
        if self.r.hmset(key, values) == True:
            self.r.zadd(self.index_key + table, key, self.hashcode(k))
            return True
        return False


    def delete(self, table, key):
        table_name = self.index_key
        if table != None:
            key = table + '_' + key
            table_name += table
        if self.r.delete(key) == 1 and self.r.zrem(table_name, key) == 1:
            return 1
        return 0

    def scan(self, table, startkey, recordcount, fields):
        table_name = self.index_key
        if table != None:
            k = startkey
            startkey = table + '_' + startkey
            table_name = table_name + table
            return msgpack.unpackb(self._scan(keys=(table_name, self.hashcode(k), recordcount, len(table)), args=fields))
        return msgpack.unpackb(self._scan(keys=(table_name, self.hashcode(startkey), recordcount, 0), args=fields))
        #keys = self.r.zrangebyscore(table_name, self.hashcode(startkey), '+inf', 0, recordcount)
        #result = {}
        #for k in keys:
        #    result[k[len(table)+1:]] = dict(zip(fields, self.r.hmget(k, fields)))
        #return result
       
    def getkeys(self, table, hashkey):
        keys = self.r.keys(table + "_" + hashkey + "*")
        num = len(table)+1
        return map(lambda i: i[num:], keys)

    def update(self, table, key, values):
        if table != None:
            key = table + '_' + key
        return self.r.hmset(key, values)

    def disconnect(self):
        if self.pool:
            return self.pool.disconnect()

    def setpids(self):
        server = dict(list(x.split(':') for x in self.r.execute_command('info', 'server').splitlines(False)[1:]))
        self.pids = [server['process_id']]

    @database.monitor_decorator
    def monitor(self, status):
        if self.r:
            #all_text = self.r.execute_command("info", "all")
            memory = dict(list(x.split(':') for x in self.r.execute_command("info", "memory").splitlines(False)[1:]))
            status.memory.current_storage = int(memory['used_memory_rss']) / 1024 / 1024
            #client_text = self.r.execute_command("info", "clients")
            #persistence_text = self.r.execute_command("info", "persistence")
            #stat_text = self.r.execute_command("info", "stats")
            #replication_text = self.r.execute_command("info", "replication")
            #cpu_text = self.r.execute_command("info", "cpu")
            #cmd_text = self.r.execute_command("info", "commandstats")
            #return memory_text
