#!/usr/bin/env python
# -*- coding: utf-8 -*-
from hbase import Hbase
from hbase.ttypes import *
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from db import database
from timeout import timeout, TimeoutError
import getpass
import sys

class HbaseDriver(database):
    client = None
    transport = None
    @database.init_decorator
    def __init__(self, host='localhost', port=9090, passwd=None, logpath=None, configpath=None, driver_only=False):
        self.transport = TSocket.TSocket(host, port)
        self.transport = TTransport.TBufferedTransport(self.transport)
        protocol = TBinaryProtocol.TBinaryProtocol(self.transport)
        self.client = Hbase.Client(protocol)
        self.transport.open()
        #if not configpath:
        #    configpath = 'nodeconfig'
        #f = open(configpath)

    def drop(self, table):
        try:
            timeout(5)(self.client.disableTable)(table)
            timeout(5)(self.client.deleteTable)(table)
        except IOError:
            pass
        except TimeoutError as e:
            print >> sys.stderr, "HBASE drop:", e
            print >> sys.stderr, "We Recommend you to restart thrift server"


    def disconnect(self):
        if self.transport.isOpen():
            self.transport.close()

    def read(self, table, key, fields=None, withfields=False):
        try:
            row = self.client.getRowWithColumns(table, key, fields, {})
            if len(row) == 0:
                if withfields:
                    return {}
                return []
            row = row[0]
        except (Thrift.TApplicationException, IOError) as e:
            if withfields:
                return {}
            return []
        if withfields:
            return {k[:-1]:v.value for k, v in row.columns.items()}
        return [i.value for i in row.columns.values()]

    def insert(self, table, key, values):
        mutationValues = []
        for k, v in values.items():
            mutationValues.append(Mutation(column=k, value=v))
        try:
            self.client.mutateRow(table, key, mutationValues, {})
        except IOError:
            try:
                keys = map(ColumnDescriptor, values.keys())
                self.client.createTable(table, keys)
            except (IOError, Hbase.AlreadyExists) as e:
                # prevent table exist error
                pass
            try:
                self.client.mutateRow(table, key, mutationValues, {})
            except IOError:
                pass


    def delete(self, table, key):
        try:
            self.client.deleteAllRow(table, key, {})
        except IOError:
            return 0
        return 1
    
    def scan(self, table, startkey, recordcount, fields):
        d = None
        l = None
        try:
            d = timeout()(self.client.scannerOpen)(table, startkey, fields, {})
        except IllegalArgument:
            return {}
        except (TApplicationException, TimeoutError) as e:
            print >> sys.stderr, "HBASE scan:", e
            print >> sys.stderr, "We recommend you to restart the thrift server."
            return {}
        except IOError:
            return {}

        try:
            l = self.client.scannerGetList(d, recordcount)
        except IllegalArgument:
            self.client.scannerClose(d)
            return {}
        except TApplicationException:
            return {}
        self.client.scannerClose(d)
        result = {}
        for i in l:
            s = result[i.row] = {}
            for k in fields:
                v = i.columns.get(k + ':')
                if v:
                    s.__setitem__(k, i.columns.get(k + ':').value)
                else:
                    s.__setitem__(k, None)
        return result

    def update(self, table, key, values):
        self.insert(table, key, values)

    def getkeys(self, table, hashkey):
        l = {}
        d = None
        try:
            d = timeout()(self.client.scannerOpenWithPrefix)(table, hashkey, [], {})
        except (TApplicationException, TimeoutError) as e:
            print >> sys.stderr, "HBASE getkeys:", e
            print >> sys.stderr, "We recommend you to restart the thrift server."
            return []
        except IOError:
            return []

        result = []
        while True:
            try:
                l = self.client.scannerGetList(d, 1000)
            except IllegalArgument:
                return []
            except (IOError, TApplicationException) as e:
                break
            if not l:
                break
            map(lambda i: result.append(i.row), l)
        self.client.scannerClose(d)
        return result

    def setpids(self):
        f = open('/tmp/hbase-%s-master.pid' % getpass.getuser())
        self.pids = [int(f.read())]
        f.close()

    @database.monitor_decorator
    def monitor(self, status):
        pass
