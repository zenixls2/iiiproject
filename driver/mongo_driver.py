#!/usr/bin/env python
# -*- coding: utf-8 -*-
from db import database
import coordinator.heartbeat_pb2 as heart
import re
import pymongo
import os
import time
from bson.code import Code
import bson
class MongoDriver(database):
    m = None
    dbase = None
    tb = None
    prev_table = None

    @database.init_decorator
    def __init__(self, host='localhost', port=27017, passwd=None, logpath=None, configpath=None, driver_only=False):
        self.m = pymongo.MongoClient(host, port)
        self.m.write_concern = {'w': 1, 'fsync': False}
        self.dbase = self.m['database']

    def _getDB(self, table):
        if self.prev_table != table:
            self.prev_table = table
            self.tb = self.dbase[table]
    
    def drop(self, table):
        self._getDB(table)
        self.tb.drop()

    def read(self, table, key, fields=None, withfields=False):
        self._getDB(table)
        # support get by fields temporary
        result = {}
        # fields: list
        f = {"_id": False}
        
        if fields:
            for i in fields:
                f[i] = True
        if withfields:
            ret = self.tb.find_one(spec_or_id={"_id": key}, fields=f)
            if ret == None:
                return {}
            return ret
        ret = self.tb.find_one(spec_or_id={"_id": key}, fields=f)
        if ret == None:
            return []
        return ret

    # values: map
    def insert(self, table, key, values): 
        self._getDB(table)
        try:
            values["_id"] = key
            return self.tb.insert(values)
        except pymongo.errors.DuplicateKeyError:
            del values['_id']
            return self.tb.update({"_id": key}, {"$set": values})

    def delete(self, table, key):
        self._getDB(table)
        return self.tb.remove({"_id": key})['n']

    def scan(self, table, startkey, recordcount, fields):
        self._getDB(table)
        result = {}
        f = {"_id": True}
        for i in fields:
            f[i] = True
        a = self.tb.find(spec={"_id": {"$gte": startkey}}, fields=f, limit=recordcount)
        try:
            while True:
                val = a.next()
                key = val.pop("_id", 0)
                result[key] = val
        except StopIteration:
            return result

    def update(self, table, key, values):
        self._getDB(table)
        try:
            self.tb.update({"_id": key}, {"$set": values})
        except bson.errors.InvalidStringData:
            print(values)
        return True

    def getkeys(self, table, hashkey):
        self._getDB(table)
        #mapper = Code("""
        #    function() {
        #        for (var key in this) {
        #            emit(key, null);
        #        }
        #    }""")
        #reducer = Code("""
        #    function(key, stuff) { return null;}""")
        #keys = self.tb.map_reduce(mapper, reducer, out = {'inline': 1}, full_response = True)
        #return map(lambda x: x['_id'], keys['results'])
        result = []
        a = self.tb.find(spec={"_id": {"$regex": "^%s" % hashkey}}, fields={"_id": True})
        return map(lambda i: i['_id'], a)

    def disconnect(self):
        if self.m and self.m.alive():
            self.m.disconnect()

    def setpids(self):
        with os.popen("pgrep mongod") as f:
            self.pids = map(int, f.read().splitlines())

    @database.monitor_decorator
    def monitor(self, status):
        if self.dbase:
            serverStatus = self.dbase.command("serverStatus")
            hostInfo = self.dbase.command('hostInfo')
            #status.memory.total_storage = hostInfo['system']['memSizeMB']
            #status.memory.current_storage = serverStatus['mem']['virtual']
            # Network read / write speed in bytes / sec for mongodb only
            """
            tmptime = time.clock()
            if self.last_network.timestamp:
                nbyin = serverStatus['network']['bytesIn']
                nbyout = serverStatus['network']['bytesOut']
                print(nbyin, nbyout)
                status.network.read_speed = \
                    int((nbyin - self.last_network.read) / (tmptime - self.last_network.timestamp))
                status.network.write_speed = \
                    int((nbyout - self.last_network.write) / (tmptime - self.last_network.timestamp))
                self.last_network.read = nbyin
                self.last_network.write = nbyout
                self.last_network.timestamp = tmptime
            else:
                self.last_network.read = serverStatus['network']['bytesIn']
                self.last_network.write = serverStatus['network']['bytesOut']
                self.last_network.timestamp = tmptime
            """
