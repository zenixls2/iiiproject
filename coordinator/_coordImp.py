import coordinator.coord_pb2 as coord
from tool import *
import time
import thread

class CoordImp(coord.CoordService):
    driver = None
    nullResponse = coord.CoordResponse()
    def getDriver(self, driver):
        self.driver = driver

    def read(self, controller, request, done):
        table = request.table
        key = request.key
        fields = request.fields
        assert self.driver != None
        vals = self.driver.read(table, key, fields)
        response = coord.CoordResponse()
        convert_all_to_bytes(vals, response.values, response.value_types)
        done.run(response)

    def insert(self, controller, request, done):
        table = request.table
        key = request.key
        values = {}
        convert_all_to_native(values, request.fields, request.values, request.value_types)
        assert self.driver != None
        ret = self.driver.insert(table, key, values)
        done.run(self.nullResponse)

    def delete(self, controller, request, done):
        table = request.table
        key = request.key
        assert self.driver != None
        ret = self.driver.delete(table, key)
        response = coord.CoordResponse()
        response.status = bool(ret)
        done.run(response)

    def scan(self, controller, request, done):
        table = request.table
        startkey = request.key
        recordcount = request.recordcount
        fields = request.fields
        assert self.driver != None
        ret = self.driver.scan(table, startkey, recordcount, fields)
        response = coord.CoordResponse()
        for k, v in ret.items():
            m = response.scan_result.add()
            m.key = k
            m.fields.extend(v.keys())
            convert_all_to_bytes(v.values(), m.values, m.value_types) 
        done.run(response)

    def update(self, controller, request, done):
        table = request.table
        key = request.key
        values = {}
        convert_all_to_native(values, request.fields, request.values, request.value_types)
        assert self.driver != None
        ret = self.driver.update(table, key, values)
        done.run(self.nullResponse)

    def disconnect(self, controller, request, done):
        pass
    def shutdown(self, controller, request, done):
        pass

    def merge(self, controller, request, done):
        def func(self, request, done):
            table = request.table
            hashkey = request.hashkey
            fields = request.fields
            target = request.target
            assert self.driver != None
            ret = self.driver.merge(table, hashkey, fields, target)
            time.sleep(10)
            done.run(self.nullResponse)
        thread.start_new_thread(func, (self, request, done))
