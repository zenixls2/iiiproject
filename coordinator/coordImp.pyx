import coordinator.coord_pb2 as coord
import time
import thread
cimport socketrpc
import msgpack
from socketrpc.server cimport Callable

class CoordImp(coord.CoordService):
    driver = None
    nullResponse = coord.CoordResponse()
    def getDriver(self, driver):
        self.driver = driver
    
    def disconnect(self, controller, request, done):
        """
        disconnect RPC call to the database
        no use anymore
        Parameter:
         - controller: (RpcController)
         - request: (NullRequest)
         - done: runable callback function
        """
        pass

    def shutdown(self, controller, request, done):
        """
        shutdown RPC call to the database
        no use any more
        Parameter:
         - controller: (RpcController)
         - request: (NullRequest)
         - done: runable callback function
        """
        pass

    def migrate(self, controller, request, done):
        """
        migration RPC call to the destination node
        @return - (CoordResponse)
        Parameter:
         - controller: (RpcController)
         - request: (CoordRequest)
         - done: runable callback function
        """
        response = coord.CoordResponse()
        tk = msgpack.unpackb(request.tk)
        target = request.target
        assert self.driver != None
        ret = self.driver.migrate(tk, target)
        response.result = msgpack.packb(ret)
        done.run(response)
        #thread.start_new_thread(func, (self, request, done))
