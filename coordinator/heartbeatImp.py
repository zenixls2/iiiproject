import coordinator.heartbeat_pb2 as heart
import time

class HeartImp(heart.HeartbeatService):
    """
    Heartbeat RPC calls definition.
    """
    driver = None
    def getDriver(self, driver):
        """
        Called to assign driver when node.py is on.
        Parameter:
         - driver: wrapped driver for specific database.(database)
        """
        self.driver = driver

    def getHeartbeat(self, controller, request, done):
        """
        RPC call to get monitoring results.
        Parameter:
         - controller: (RpcController)
         - request: (Request)
         - done: callback runnable function
        """
        response = self.driver.monitor()
        done.run(response)
