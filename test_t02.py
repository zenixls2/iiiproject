import sys
from dbtypes import *
import types
import time

print("unit: millisecond")

def operation(toprint, callee, *args):
    r = None
    t = time.time()
    r = callee(*args)
    if toprint:
        print(str(int(t * 1000000)).ljust(8) + "|" + 
          str(int((time.time() - t) * 1000000)).ljust(6) + " " + 
          callee.__name__.ljust(7) + ": " + str(r))
    return r

def validate(msg, to_valid, valid_val):
    print("\t" + msg + ": " + str(to_valid == valid_val))

table = "test"

a = AttilaDriver()

a.drop(table)
class Product:
    price = 0
    name = ""
    stock = 0
    def __init__(self, p, n, s):
        self.price = p
        self.name = n
        self.stock = s

    def hash(self, k):
        return str(ord(k[-1]))

    def encode(self):
        return table, self.hash(self.name), {"price": str(self.price), "stock": str(self.stock)}

p = Product(200, "PC", 10000)
a.insert(*p.encode())
p = Product(300, "laptop", 1000)
a.insert(*p.encode())
print a.read(table, p.hash("PC"), ["price", "stock"], True)
print a.read(table, p.hash("laptop"), ["price", "stock"], True)
print 0, a.listkeys(0, table)
print 1, a.listkeys(1, table)
print 2, a.listkeys(2, table)
a.migrate(1, 0, 1)
print 0, a.listkeys(0, table)
print 1, a.listkeys(1, table)
print 2, a.listkeys(2, table)
#a.migrate(1, 1, 2)
#print 0, a.listkeys(0, table)
#print 1, a.listkeys(1, table)
#print 2, a.listkeys(2, table)
#a.drop(table)
a.disconnect()
exit()
