from driver.mongo_driver import MongoDriver
from driver.redis_driver import RedisDriver
from driver.hbase_driver import HbaseDriver
from driver.attila_driver import AttilaDriver

"""
Define each driver wrapper here.
AttilaDriver not ready for migration operations.
"""
dbs = {"mongodb": MongoDriver, "redis": RedisDriver, "hbase": HbaseDriver, "attila": AttilaDriver}

