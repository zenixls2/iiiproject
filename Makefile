CC=gcc
CFLAGS=-shared -pthread -fwrapv -fPIC -O2 -Wall -fno-strict-aliasing -I/usr/include/python2.7 -lpython2.7 -lboost_python -lrt
.DEFAULT_GOAL:= all
proto:
	protoc coordinator/coord.proto --python_out=.
	protoc coordinator/trans.proto --python_out=.
	protoc coordinator/heartbeat.proto --python_out=.
	protoc socketrpc/rpc.proto --python_out=.

time: socketrpc/systime.pyx
	cython -a socketrpc/systime.pyx
	$(CC) $(CFLAGS) -o socketrpc/systime.so socketrpc/systime.c

coordimp: proto coordinator/coordImp.pyx
	cython -a coordinator/coordImp.pyx
	$(CC) $(CFLAGS) -o coordinator/coordImp.so coordinator/coordImp.c

server: socketrpc/server.pyx
	cython -a socketrpc/server.pyx
	$(CC) $(CFLAGS) -o socketrpc/server.so socketrpc/server.c

channel: socketrpc/channel.pyx
	cython -a socketrpc/channel.pyx
	$(CC) $(CFLAGS) -o socketrpc/channel.so socketrpc/channel.c
controller: socketrpc/controller.pyx
	cython -a socketrpc/controller.pyx
	$(CC) $(CFLAGS) -o socketrpc/controller.so socketrpc/controller.c

all: time coordimp server channel controller

clean:
	rm coordinator/*.c
	rm coordinator/*.so
	rm socketrpc/*.c
	rm socketrpc/*.so

rmlog:
	rm log/*
