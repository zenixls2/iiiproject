import time
from multiprocessing import Value, Lock, RLock
from ctypes import c_double, c_int
import msgpack
import coordinator.coord_pb2 as coord
import coordinator.heartbeat_pb2 as heartbeat
import socketrpc.channel
import socketrpc.controller
import dbtypes
import logging
from tables import TABLES
'''Caching groups'''

caching = {}
MAX_RANGE = 2000

class Group(object):
    def __init__(self):
        self.lock = RLock()
        self.wlock = RLock()
        self.target = Value(c_int, -1, lock=None)
        self.count = Value(c_int, 0, lock=None)

def do_hash(key):
    """
    hash the key to the grouping index
    @return - returns a list containing the hashed key. Other columns are optional,
    Parameter:
     - key: original key (str)
    """
    return (key[:-2], key[-2:])

def get_group(table, key):
    """
    get group instance mapping to specific table and key.
    @return - returns a group instance
    Parameter:
     - table: table name (str)
     - key: original key (str)
    """
    global caching
    k, v = do_hash(key)
    try:
        return caching[table + '_' + k]
    except KeyError:
        return None

def iter_group(k):
    """
    an iterator to all elements of a group.
    Parameter:
     - k: hashed key get from do_hash. (str)
    """
    for i in xrange(100):
        yield k + str(i)

def clear_groups(table):
    """
    clear all groups in caching global data structure.
    Parameter:
     - table: table name (str)
    """
    global caching
    for k, v in caching.iteritems():
        if k.startswith(table):
            with v.lock:
                v.target.value = -1
                v.count.value = 0

def filter_groups(table, startkey, recordcount):
    """
    Get the number of items to scan with for each node.
    Make sure that group.count has the right value.
    @return - the numbers of items to scan per node (dict)
    Paramter:
     - table: table name (str)
     - startkey: key to start scan from (str)
     - recordcount: total numbers of items to find out. (int)
    """
    global caching
    keycount = {}
    total = 0
    length = len(table)+1
    sk = do_hash(startkey)[0]
    for k ,v in caching.iteritems():
        if k.startswith(table) and k[length:] >= sk:
            with v.lock:
                i, d = (v.target.value, v.count.value)
                if i != -1:
                    total += d
                    if total > recordcount:
                        d -= total - recordcount
                    try:
                        keycount[i] += d
                    except KeyError:
                        keycount[i] = d
                    if total >= recordcount:
                        break
    return keycount

def init_groups():
    """
    Initialize all groups in caching global data structure.
    NEVER USE THIS AFTER THE SERVER SOCKET STARTING LISTENING AND FORKING NEW PROCESSES.
    """
    global caching
    global MAX_RANGE
    global TABLES
    for t in TABLES:
        for i in xrange(MAX_RANGE):
            caching[t + '_' + str(i)] = Group()
        caching[t + '_'] = Group()

class NodeInfo(object):
    def __init__(self, name, myid, host, rpc_port, driver_type, port, operable):
        """
        Node information constructor. Init logging.
        Parameter:
         - name: node name (str)
         - myid: node id (int)
         - host: server address (str)
         - rpc_port: rpc protocol (int)
         - driver_type: redis | hbase | mongodb, (str)
         - port: driver port (int)
         - operable: should operations redirect to the node. (bool)
        """
        self.name = name
        self.rpc_port = rpc_port
        self.port = port
        self.ip = host
        self.driver_type = driver_type
        self.id = myid
        self.disks = {}
        self.cpus = {}
        self.network = None
        self.last_monitor_time = time.time()
        self.operable = operable
        
        self.read_avg = Value(c_double, 0., lock=None)
        self.insert_avg = Value(c_double, 0., lock=None)
        self.scan_avg = Value(c_double, 0., lock=None)
        self.update_avg = Value(c_double, 0., lock=None)
        self.delete_avg = Value(c_double, 0., lock=None)
        self.getkeys_avg = Value(c_double, 0., lock=None)

        self.readlock = RLock()
        self.insertlock = RLock()
        self.scanlock = RLock()
        self.updatelock = RLock()
        self.deletelock = RLock()
        self.getkeyslock = RLock()
        
        self.read_latency = Value(c_double, 0., lock=None)
        self.insert_latency = Value(c_double, 0., lock=None)
        self.scan_latency = Value(c_double, 0., lock=None)
        self.update_latency = Value(c_double, 0., lock=None)
        self.delete_latency = Value(c_double, 0., lock=None)
        self.getkeys_latency = Value(c_double, 0., lock=None)

        self.read_o = Value(c_int, 0, lock=None)
        self.insert_o = Value(c_int, 0, lock=None)
        self.scan_o = Value(c_int, 0, lock=None)
        self.update_o = Value(c_int, 0, lock=None)
        self.delete_o = Value(c_int, 0, lock=None)
        self.getkeys_o = Value(c_int, 0, lock=None)

        print host, rpc_port

        channel = socketrpc.channel.RpcChannel(host, rpc_port)
        heartbeatChannel = socketrpc.channel.RpcChannel(host, rpc_port)
        self.controller = socketrpc.controller.RpcController()
        self.service = coord.CoordService_Stub(channel)
        self.heartbeatService = heartbeat.HeartbeatService_Stub(heartbeatChannel)

        FORMAT = '"%(asctime)s",%(read_avg)d,%(insert_avg)d,%(delete_avg)d,%(scan_avg)d,%(update_avg)d,%(getkeys_avg)d'
        self.logfile = logging.FileHandler('log/%s.csv' % self.name)
        self.logfile.setFormatter(logging.Formatter(fmt=FORMAT, datefmt="%Y %m %d %H %M %S"))
        self.logger = logging.getLogger(self.name)
        self.logger.setLevel(logging.DEBUG)
        self.logger.addHandler(self.logfile)

        try:
            f = open('log/%s.info' % self.name)
            cfile = f.read()
            f.close()
            if cfile:
                self.const_info = msgpack.unpackb(cfile)
            else:
                self.const_info = {}
        except IOError:
            f = open('log/%s.info' % self.name, 'w+')
            f.close()
            self.const_info = {}

    def getdriver(self):
        """
        Must be called from the very start to allow operations to the driver
        """
        self.driver = dbtypes.dbs[self.driver_type](self.ip, self.port, driver_only=True)

    def read(self, table, key, fields, with_values):
        return self.driver.read(table, key, fields, with_values)

    def insert(self, table, key, values):
        return self.driver.insert(table, key, values)

    def update(self, table, key, values):
        return self.driver.update(table, key, values)

    def scan(self, table, startkey, recordcount, fields):
        return self.driver.scan(table, startkey, recordcount, fields)

    def delete(self, table, key):
        return self.driver.delete(table, key)

    def getkeys(self, table, key):
        return self.driver.getkeys(table, key)

    def migrate(self, num, target):
        """
        Migrate from target to me.
        Separate to two steps:
        1. read out and insert items to this node
        2. modify group target and delete the item from the origin.
        @return - True if migrate suceeded. False if not.
        Parameter:
         - num: number of groups to migrate (int)
         - target: the node to migrate from (NodeInfo)
        """
        global caching
        if not target.operable or not self.operable:
            return False
        if target.id == self.id:
            return True
        request = coord.CoordRequest()
        tk = {}
        total = 0
        for i, v in caching.iteritems():
            if total >= num:
                break
            if v.target.value == target.id:
                table, k = i.split('_')
                total += 1
                try:
                    tk[table].append(k)
                except KeyError, e:
                    tk[table] = [k]
        request.tk = msgpack.packb(tk)
        request.target = target.name
        response = self.service.migrate(self.controller, request)
        if response:
            r = msgpack.unpackb(response.result)
            for table, l in r.iteritems():
                for key in l:
                    k = table + '_' + do_hash(key)[0]
                    group = caching[k]
                    with group.wlock:
                        group.target.value = self.id
                        target.driver.delete(table, key)
            return True
        return False
           
    def monitor(self):
        """
        Get and log down the status (resource usage / performance) of this node.
        """
        response = None
        request = heartbeat.Request()
        response = self.heartbeatService.getHeartbeat(self.controller, request)
        if response:
            # Disk
            for i in response.disks:
                val = {"read_speed": i.read_speed,
                        "write_speed": i.write_speed,
                        "total_blocks": i.total_blocks,
                        "avail_blocks": i.avail_blocks,
                        "block_size": i.block_size}
                self.const_info[i.name] = {'total_blocks': i.total_blocks, 'block_size': i.block_size}
                try:
                    self.disks[i.name].debug('', extra=val)
                except KeyError, e:
                    FORMAT = '"%(asctime)s",%(read_speed)d,%(write_speed)d,%(avail_blocks)d'
                    f = logging.FileHandler('log/%s_%s.csv' % (self.name, i.name))
                    f.setFormatter(logging.Formatter(fmt=FORMAT, datefmt="%Y %m %d %H %M %S"))
                    l = logging.getLogger("%s_%s" % (self.name, i.name))
                    l.setLevel(logging.DEBUG)
                    l.addHandler(f)
                    self.disks[i.name] = l
                    l.debug('', extra=val)

            # CPU
            total = response.cpus[0]
            counter = 0
            self.const_info['cpu_num'] = len(response.cpus) - 1
            for i in response.cpus[1:]:
                val = {"user": i.user,
                        "system": i.system,
                        "idle": i.idle,
                        "iowait": i.iowait}
                try:
                    self.cpus[counter].debug('', extra=val)
                except KeyError, e:
                    FORMAT = '"%(asctime)s",%(user)d,%(system)d,%(idle)d,%(iowait)d'
                    f = logging.FileHandler('log/%s_cpu%d.csv' % (self.name, counter))
                    f.setFormatter(logging.Formatter(fmt=FORMAT, datefmt="%Y %m %d %H %M %S"))
                    l = logging.getLogger("%s_cpu%d" % (self.name, counter))
                    l.setLevel(logging.DEBUG)
                    l.addHandler(f)
                    self.cpus[counter] = l
                    l.debug('', extra=val)
                counter += 1

            # Network
            val = {"read": response.network.read_speed,
                    "write": response.network.write_speed}
            if self.network:
                self.network.debug('', extra=val)
            else:
                FORMAT = '"%(asctime)s",%(read)d,%(write)d'
                f = logging.FileHandler('log/%s_network.csv' % (self.name))
                f.setFormatter(logging.Formatter(fmt=FORMAT, datefmt='%Y %m %d %H %M %S'))
                l = logging.getLogger("%s_network" % (self.name))
                l.setLevel(logging.DEBUG)
                l.addHandler(f)
                self.network = l
                l.debug('', extra=val)

            # Memory
            self.const_info['memory'] = response.memory.total_storage
            try:
                f = open('log/%s.info' % self.name, 'r+')
                f.write(msgpack.packb(self.const_info))
                f.close()
            except IOError, e:
                f = open('log/%s.info' % self.name, 'r+')
                f.write(msgpack.packb(self.const_info))
                f.close()
            

            # avg latency
            rv = iv = sv = uv = dv = gv = 0.
            rl = il = sl = ul = dl = gl = 0.
            ro = io = so = uo = do = g0 = 0

            with self.readlock:
                rl = self.read_latency.value
                ro = self.read_o.value
                self.read_latency.value = 0.
                self.read_o.value = 0
            if ro == 0.:
                rv = self.read_avg.value
            else:
                rv = self.read_avg.value = rl * 1000000 / ro

            with self.insertlock:
                il = self.insert_latency.value
                io = self.insert_o.value
                self.insert_latency.value = 0.
                self.insert_o.value = 0
            if io == 0:
                iv = self.insert_avg.value
            else:
                iv = self.insert_avg.value = il * 1000000 / io

            with self.scanlock:
                sl = self.scan_latency.value
                so = self.scan_o.value
                self.scan_latency.value = 0.
                self.scan_o.value = 0
            if so == 0:
                sv = self.scan_avg.value
            else:
                sv = self.scan_avg.value = sl * 1000000 / so

            with self.updatelock:
                ul = self.update_latency.value
                uo = self.update_o.value
                self.update_latency.value = 0.
                self.update_o.value = 0
            if uo == 0:
                uv = self.update_avg.value
            else:
                uv = self.update_avg.value = ul * 1000000 / uo

            with self.deletelock:
                dl = self.delete_latency.value
                do = self.delete_o.value
                self.delete_latency.value = 0.
                self.delete_o.value = 0
            if do == 0:
                dv = self.delete_avg.value
            else:
                dv = self.delete_avg.value = dl * 1000000 / do

            with self.getkeyslock:
                gl = self.getkeys_latency.value
                go = self.getkeys_o.value
                self.getkeys_latency.value = 0.
                self.getkeys_o.value = 0
            if go == 0:
                gv = self.getkeys_avg.value
            else:
                gv = self.getkeys_avg.value = gl * 1000000 / go

            d = {'read_avg': rv,
                 'insert_avg': iv,
                 'delete_avg': dv,
                 'scan_avg': sv,
                 'update_avg': uv,
                 'getkeys_avg': gv}
            self.logger.debug('', extra=d)
            return response

def get_best_node(node_counters):
    """
    Get best node to insert new item.
    @return - node id with lowest score (user defined score) (int)
    Parameter:
     - node_counters: node informations (NodeInfo)
    """
    scores = {}
    for i in node_counters:
        if i.operable:
            score = 0.
            score += i.insert_avg.value
            score += i.update_avg.value
            score += i.read_avg.value
            score += i.scan_avg.value
            score += i.getkeys_avg.value
            scores[i.id] = score
    smallest = scores[0]
    smallestid = 0
    for k, v in scores.iteritems():
        if smallest > v:
            smallest = v
            smallestid = k
    return smallestid
    #return sorted(scores.iteritems(), key = lambda s: s[1])[0][0]


'''Operations'''

def migrate(node_counters, req):
    """
    Migrate from node target to node tid.
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    starttime = time.time()
    num = req[1]
    target = req[2]
    tid = req[3]
    response = node_counters[tid].migrate(num, node_counters[target])
    return response

def getkeys(node_counters, req):
    """
    Get keys for specific table and start with k as prefix
    @return - key list
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    table = req[1]
    k = req[2]
    result = []
    for i in node_counters:
        if i.operable:
            result += i.getkeys(table, k)
    return result

def read(node_counters, req):
    """
    Read item.
    @return - dict of field, value pairs if withfields is on, and list of values if off.
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    starttime = time.time()
    table = req[1]
    key = req[2]
    fields = req[3]
    withfields = req[4]
    target = None
    group = get_group(table, key)
    if group == None:
        return []
    target = group.target.value
    if target == -1:
        return []
    node = node_counters[target]
    #with group.lock:
    #    group.freq.value += 1
    response = node.read(table, key, fields, withfields)
    with node.readlock:
        node.read_o.value += 1
        node.read_latency.value += time.time() - starttime
    return response

def insert(node_counters, req):
    """
    Insert item
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    starttime = time.time()
    table = req[1]
    key = req[2]
    values = req[3]
    target = None
    group = get_group(table, key)
    #with group.lock:
    #    group.freq.value += 1
    with group.wlock:
        # TODO: an evaluation function to get target
        with group.lock:
            target = group.target.value
            if target == -1:
                target = get_best_node(node_counters)
            group.target.value = target
            group.count.value += 1
        node = node_counters[target]
        response = node.insert(table, key, values)
    with node.insertlock:
        node.insert_o.value += 1
        node.insert_latency.value += time.time() - starttime
    return response

def update(node_counters, req):
    """
    Update the value of specific item.
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    starttime = time.time()
    table = req[1]
    key = req[2]
    values = req[3]
    group = get_group(table, key)
    if group == None:
        return []
    #with group.lock:
    #    group.freq.value += 1
    with group.wlock:
        target = group.target.value
        target = int(target)
        if target == -1:
            return []
        node = node_counters[target]
        response = node.update(table, key, values)

    with node.updatelock:
        node.update_o.value += 1
        node.update_latency.value += time.time() - starttime


def scan(node_counters, req):
    """
    Scan through nodes
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    starttime = time.time()
    table = req[1]
    startkey = req[2]
    recordcount = req[3]
    fields = req[4]
    response = {}
    keycount = {}
    keycount = filter_groups(table, startkey, recordcount)
    for target, val in keycount.iteritems():
        node = node_counters[target]
        r = node.scan(table, startkey, val, fields)
        response.update(r)
        with node.scanlock:
            node.scan_o.value += 1
            node.scan_latency.value += time.time() - starttime
    return response


def delete(node_counters, req):
    """
    Delete key
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    starttime = time.time()
    table = req[1]
    key = req[2]
    group = get_group(table, key)
    if group == None:
        return []
    with group.wlock:
        with group.lock:
            target = group.target.value
            target = int(target)
            if target == -1:
                return []
            node = node_counters[target]
            response = node.delete(table, key)
            if response == 1:
                pass
            else:
                group.target.value = -1
                group.count.value -= 1
                if group.count.value <= 0:
                    group.count.value = 0
    with node.deletelock:
        node.delete_o.value += 1
        node.delete_latency.value += time.time() - starttime
    return response

def drop(node_counters, req):
    """
    Drop table
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    table = req[1]
    if table:
        for i in node_counters:
            i.driver.drop(table)
        clear_groups(table)
    return True

def listkeys(node_counters, req):
    """
    List all keys of specific node
    @return - list of keys (list)
    Parameter:
     - node_counters: node information data structure (NodeInfo)
     - req: request argument list (list)
    """
    nodeid = req[1]
    table = req[2]
    return node_counters[nodeid].getkeys(table, "")

def getstatus(node_counters, req):
    nid = req[1]
    request = heartbeat.Request()
    response = str(node_counters[nid].heartbeatService.getHeartbeat(node_counters[nid].controller, request))
    return response
